<?php

namespace Drupal\maintenance_extended\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\system\Form\SiteMaintenanceModeForm;

/**
 * Provides the extended 'Maintenance Mode' form.
 */
class MaintenanceExtendedForm extends SiteMaintenanceModeForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    // Load the config.
    $config = $this->config('system.maintenance');

    // Retrieve the original form.
    $form = parent::buildForm($form, $form_state);

    // Maintenance Mode - Title.
    $form['maintenance_mode_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title to display when in maintenance mode'),
      '#default_value' => $config->get('title') ?? $this->t('Site under maintenance'),
      '#weight' => 1,
    ];

    // Maintenance Mode - Message.
    // (Convert to 'text_format' field and set weight)
    $form['maintenance_mode_message']['#type'] = 'text_format';
    $form['maintenance_mode_message']['#format'] = $config->get('message')['format'] ?? NULL;
    $form['maintenance_mode_message']['#default_value'] = $config->get('message')['value']  ?? NULL;
    $form['maintenance_mode_message']['#weight'] = 2;
    // Remove the #config_target which was added in Drupal Core 10.2.x,
    // as it doesn't seem to support 'text_format' type yet.
    unset($form['maintenance_mode_message']['#config_target']);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    // Set the title in config.
    $this->config('system.maintenance')->set('title', $form_state->getValue('maintenance_mode_title'));
    // Set the message in config.
    $this->config('system.maintenance')->set('message', $form_state->getValue('maintenance_mode_message'));
    // Save the config.
    $this->config('system.maintenance')->save();

    parent::submitForm($form, $form_state);
  }

}
