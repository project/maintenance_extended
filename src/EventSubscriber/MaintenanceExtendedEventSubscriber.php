<?php

namespace Drupal\maintenance_extended\EventSubscriber;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Render\BareHtmlPageRendererInterface;
use Drupal\Core\Site\MaintenanceModeEvents;
use Drupal\Core\Site\MaintenanceModeInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;

/**
 * Subscribes to maintenance mode events to provide extended functionality.
 */
class MaintenanceExtendedEventSubscriber implements EventSubscriberInterface {

  /**
   * The maintenance mode.
   *
   * @var \Drupal\Core\Site\MaintenanceModeInterface
   */
  protected MaintenanceModeInterface $maintenanceMode;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $config;

  /**
   * The bare HTML page renderer.
   *
   * @var \Drupal\Core\Render\BareHtmlPageRendererInterface
   */
  protected BareHtmlPageRendererInterface $bareHtmlPageRenderer;

  /**
   * Constructs a new MaintenanceExtendedEventSubscriber.
   *
   * @param \Drupal\Core\Site\MaintenanceModeInterface $maintenance_mode
   *   The maintenance mode.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Render\BareHtmlPageRendererInterface $bare_html_page_renderer
   *   The bare HTML page renderer.
   */
  public function __construct(MaintenanceModeInterface $maintenance_mode, ConfigFactoryInterface $config_factory, BareHtmlPageRendererInterface $bare_html_page_renderer) {
    $this->maintenanceMode = $maintenance_mode;
    $this->config = $config_factory;
    $this->bareHtmlPageRenderer = $bare_html_page_renderer;
  }

  /**
   * Returns response when site is in maintenance mode and user is not exempt.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event to process.
   */
  public function onMaintenanceModeRequest(RequestEvent $event): void {
    // Load the request from the event.
    $request = $event->getRequest();

    // Load title and message from config.
    $title = $this->config->get('system.maintenance')->get('title');
    $message = new FormattableMarkup($this->config->get('system.maintenance')->get('message')['value'], [
      '@site' => $this->config->get('system.site')->get('name'),
    ]);

    // Response is different based on the request format.
    if ($request->getRequestFormat() !== 'html') {
      $response = new Response($message, 503, ['Content-Type' => 'text/plain']);
      // Calling RequestEvent::setResponse() also stops propagation of event.
      $event->setResponse($response);
      return;
    }
    drupal_maintenance_theme();
    $response = $this->bareHtmlPageRenderer->renderBarePage(['#markup' => $message], $title, 'maintenance_page');
    $response->setStatusCode(503);
    // Calling RequestEvent::setResponse() also stops propagation of the event.
    $event->setResponse($response);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    // Make sure our subscriber gets triggered before
    // Core's MaintenanceModeSubscriber.
    $events[MaintenanceModeEvents::MAINTENANCE_MODE_REQUEST][] = [
      'onMaintenanceModeRequest', -999,
    ];

    return $events;
  }

}
