<?php

namespace Drupal\maintenance_extended\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;
use Drupal\maintenance_extended\Form\MaintenanceExtendedForm;

/**
 * Alters the system site maintenance route to use the MaintenanceExtendedForm.
 */
class MaintenanceExtendedRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    if ($route = $collection->get('system.site_maintenance_mode')) {
      $route->setDefault('_form', MaintenanceExtendedForm::class);
    }
  }

}
