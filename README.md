This module extends the existing `Maintenance mode` page of Drupal Core.

It provides the following functionality:
* A `Title` field to alter the title of the `Maintenance mode` page
  (By default, this value is fixed in code and not changeable).
* The `Message` field now uses a `text_format` field,
  allowing you to use a rich text editor, eg. `ckeditor`
  (By default this was a `textarea` field).
